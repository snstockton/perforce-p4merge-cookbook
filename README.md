# p4merge cookbook

Installs Perforce visual tools, specifically P4Merge.

TODO: This is yet to be tested and only supports Windows...

# Requirements
Windows

# Usage

# Attributes

* `['p4merge']['url']` - The URL to the downloadable installer.
* `['p4merge']['checksum']` - The installer file SHA256 checksum.
* `['p4merge']['package_name']` - The installer package name once installed.

# Recipes

default
-------

Installs Perforce visual tools.

# Author

Author:: Shawn Neal
