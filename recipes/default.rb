#
# Cookbook Name:: p4merge
# Recipe:: default
#
# Copyright (C) 2013 Daptiv Solutions LLC
# 
# All rights reserved - Do Not Redistribute
#

url = node['p4merge']['url']
checksum = node['p4merge']['checksum']
package_name = node['p4merge']['package_name']


windows_package package_name do
  source url
  checksum checksum
  action :install
end
